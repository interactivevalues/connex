<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\ImageService;

class JsonController
{

    protected $parentCategory = 0;
    protected $page = 0;

    public function run()
    {
        error_reporting(E_ERROR | E_PARSE);

        $data = [];
        $this->parentCategory = (int)GeneralUtility::_GET('category');
        $this->page = (int)GeneralUtility::_GET('page');

        if ($this->parentCategory > 0) {
            $rows = $this->getRows();
            $data = ['posts' => $rows];
        }
        $this->sendOutput($data);
    }

    protected function getRows()
    {
        // Get all subcategories
        $subCategories = [$this->parentCategory];
        $subCategoriesTmp = $this->getDatabaseConnection()->exec_SELECTgetRows('uid,title', 'sys_category',
            'deleted=0 AND hidden=0 AND parent=' . $this->parentCategory);
        foreach ($subCategoriesTmp as $item) {
            $subCategories[] = $item['uid'];
        }

        $rows = $this->getDatabaseConnection()->exec_SELECTgetRows(
            'news.uid,news.title,news.title2 as subline,news.format',
            'tx_news_domain_model_news as news
                RIGHT JOIN sys_category_record_mm on news.uid=sys_category_record_mm.uid_foreign',
            'news.deleted=0 AND news.hidden=0 AND sys_category_record_mm.uid_local IN(' . implode(',',
                $subCategories) . ')
               GROUP BY news.uid
                '
        );

        $linkConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['link_generation']);
        $mediaFields = ['media_header', 'media_landscape', 'media_square'];
        $imageService = self::getImageService();

        foreach ($rows as &$row) {
            $row['title'] = str_replace(LF, '<br>', $row['title']);
            $row['subline'] = str_replace(LF, '<br>', $row['subline']);
            $row['format'] = $row['format'] == 0 ? 'quer' : 'quadratisch';

            $urlToFetchLink = GeneralUtility::getIndpEnv('TYPO3_SITE_URL')
                . 'index.php?eID=link&pageId=' . $this->page . '&secret=' . $linkConfig['secret']
                . '&arguments=' . base64_encode(sprintf('&tx_news_pi1[news]=%s&tx_news_pi1[controller]=News&tx_news_pi1[action]=detail',
                    $row['uid']));

            $url = GeneralUtility::getUrl($urlToFetchLink);
            $row['link'] = $url;

            $images = $this->getDatabaseConnection()->exec_SELECTgetRows(
                'sys_file_reference.*',
                'sys_file LEFT JOIN sys_file_reference ON sys_file_reference.uid_local = sys_file.uid',
                'sys_file_reference.deleted=0 AND sys_file_reference.tablenames = "tx_news_domain_model_news" AND sys_file_reference.uid_foreign = ' . $row['uid'],
                '',
                '',
                '',
                'fieldname'
            );

            if (!empty($images)) {
                foreach ($images as $field => $image) {
                    unset($GLOBALS['TSFE']);
                    $image = $this->getFalByReferenceUid($image['uid']);
                    if (is_null($image)) {
                        continue;
                    }

                    $image = $imageService->getImage($src, $image, $treatIdAsReference);
                    $processingInstructions = array(
                        'crop' => $image->getProperty('crop'),
                    );

                    switch ($field) {
                        case 'media_header':
                            $processingInstructions['maxWidth'] = 1200;
                            break;
                        case 'media_landscape':
                            $processingInstructions['width'] = '800c';
                            $processingInstructions['height'] = '400c';
                            break;
                        case 'media_square':
                            $processingInstructions['width'] = '400c';
                            $processingInstructions['height'] = '400c';
                            break;
                    }

                    $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
                    $urlToImage = $imageService->getImageUri($processedImage, true);
                    $row['image_' . $field] = $urlToImage;
                }
            }

            $categories = $this->getDatabaseConnection()->exec_SELECTgetRows(
                'sys_category.title, sys_category.uid, sys_category.parent',
                'sys_category_record_mm RIGHT JOIN sys_category on sys_category.uid=sys_category_record_mm.uid_local',
                'sys_category.parent > 0 AND sys_category_record_mm.tablenames="tx_news_domain_model_news" AND sys_category_record_mm.uid_foreign=' . $row['uid']
            );
            if (!empty($categories)) {
                $catList = [];
                foreach ($categories as $cat) {
                    $catList[] = $cat['title'];
                }
                $row['category'] = implode(',', $catList);
            }
        }
        return $rows;
    }

    protected function getMedia($table, $field, $id)
    {
        /** @var FileRepository $fileRepository */
        $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
        return $fileRepository->findByRelation($table, $field, $id);
    }

    /**
     * Return an instance of ImageService using object manager
     *
     * @return ImageService
     */
    protected function getImageService()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        return $objectManager->get(ImageService::class);
    }

    protected function getFalByReferenceUid($uid)
    {
        /** @var FileRepository $fileRepository */
        $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
        return $fileRepository->findFileReferenceByUid($uid);
    }


    protected function sendOutput(array $data)
    {
        echo json_encode($data);
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}

$json = new JsonController();
$json->run();