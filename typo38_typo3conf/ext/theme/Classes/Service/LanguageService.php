<?php
namespace InteractiveValues\Theme\Service;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Class LanguageService
 *
 * @package InteractiveValues\Theme\Service
 */
class LanguageService {

	/**
	 * Reads a locallang file.
	 *
	 * @param string $fileRef Reference to a relative filename to include.
	 * @param array $languages languages
	 * @param int $errorMode Error mode (when file could not be found): 0 - syslog entry, 1 - do nothing, 2 - throw an exception
	 * @return array Returns the $LOCAL_LANG array found in the file. If no array found, returns empty array.
	 */
	public function getTranslations($fileRef, array $languages, $errorMode = 2) {
		$localLanguage = array();

		/** @var $languageFactory \TYPO3\CMS\Core\Localization\LocalizationFactory */
		$languageFactory = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Localization\\LocalizationFactory');

		foreach ($languages as $language) {
			$tempLL = $languageFactory->getParsedData($fileRef, $language, 'utf-8', $errorMode);

			$localLanguage['default'] = $tempLL['default'];
			if (!isset($localLanguage[$language])) {
				$localLanguage[$language] = $localLanguage['default'];
			}
			if ($language !== 'default' && isset($tempLL[$language])) {
				// Merge current language labels onto labels from previous language
				// This way we have a labels with fall back applied
				ArrayUtility::mergeRecursiveWithOverrule($localLanguage[$language], $tempLL[$language], TRUE, FALSE);
			}
		}

		return $localLanguage;
	}
}