<?php
namespace InteractiveValues\Theme\ViewHelpers;
/*                                                               *
 * This file is brought to you by Georg Großberger               *
 * (c) 2015 InteractiveValues GmbH                                      *
 *                                                               *
 * It is free software; you can redistribute it and/or modify it *
 * under the terms of the MIT License / X11 License              *
 *                                                               */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Trim a passed string of white - space characters
 *
 * @author Georg Großberger <georg.grossberger@InteractiveValues.at>
 * @copyright (c) 2015 by InteractiveValues GmbH <www.InteractiveValues.at>
 */
class TrimViewHelper extends AbstractViewHelper {

	/**
	 * @param string $content
	 * @return string
	 */
	public function render($content = NULL) {
		if (empty($content)) {
			$content = (string) $this->renderChildren();
		}

		return trim($content);
	}
}
