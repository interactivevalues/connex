<?php
namespace InteractiveValues\Theme\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class LinefeedViewHelper extends AbstractViewHelper {

	/**
	 * @param string $content
	 * @return string
	 */
	public function render($content = NULL) {
		if (empty($content)) {
			$content = (string) $this->renderChildren();
		}

		//$content = str_replace(LF, ' ', $content);
		$content = str_replace(["\r\n", "\r", "\n"], " ", $content);
		return trim($content);
	}
}
