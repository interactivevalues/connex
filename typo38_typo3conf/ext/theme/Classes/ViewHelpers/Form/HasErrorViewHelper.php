<?php

namespace InteractiveValues\Theme\ViewHelpers\Form;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormFieldViewHelper;

class HasErrorViewHelper extends AbstractFormFieldViewHelper {

	/**
	 * @param string $class
	 * @return string
	 */
	public function render($class) {

		$mappingResultsForProperty = $this->getMappingResultsForProperty();
		return $mappingResultsForProperty->hasErrors() ? $class : '';
	}
}