<?php
namespace InteractiveValues\Theme\ViewHelpers;
/*                                                               *
 * This file is brought to you by Georg Großberger               *
 * (c) 2015 InteractiveValues GmbH                                      *
 *                                                               *
 * It is free software; you can redistribute it and/or modify it *
 * under the terms of the MIT License / X11 License              *
 *                                                               */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Serialize an array of any values to JSON
 *
 * @author Georg Großberger <georg.grossberger@InteractiveValues.at>
 * @copyright (c) 2015 by InteractiveValues GmbH <www.InteractiveValues.at>
 */
class JsonViewHelper extends AbstractViewHelper {

	/**
	 * @param array $variables
	 * @return string
	 */
	public function render($variables = NULL) {
		if ($variables === NULL) {
			$variables = $this->renderChildren();
		}
		return json_encode($variables);
	}
}
