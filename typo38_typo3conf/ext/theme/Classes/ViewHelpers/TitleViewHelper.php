<?php
namespace InteractiveValues\Theme\ViewHelpers;
/*                                                               *
 * This file is brought to you by Georg Großberger               *
 * (c) 2015 InteractiveValues GmbH                                      *
 *                                                               *
 * It is free software; you can redistribute it and/or modify it *
 * under the terms of the MIT License / X11 License              *
 *                                                               */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Add contents to the page title
 *
 * Useage
 *
 * Add to title
 * <t:title>Added to existing title</t:title>
 *
 * Add before title
 * <t:title mode="prepend">Title is: </t:title>
 *
 * Replace the title
 * <t:title mode="replace">New Title</t:title>
 *
 * @author Georg Großberger <georg.grossberger@InteractiveValues.at>
 * @copyright (c) 2015 by InteractiveValues GmbH <www.InteractiveValues.at>
 */
class TitleViewHelper extends AbstractViewHelper {

	/**
	 * @param string $content
	 * @param string $mode
	 */
	public function render($content = NULL, $mode = 'append') {
		if (!$content) {
			$content = $this->renderChildren();
		}

		$title = $GLOBALS['TSFE']->page['title'];

		switch ($mode) {
			case 'prepend':
				$title = $content . ' ' . $title;
				break;
			case 'replace':
				$title = $content;
				break;
			default:
				$title = $title . ' ' . $content;
		}

		$GLOBALS['TSFE']->page['title'] = trim($title);
	}
}
