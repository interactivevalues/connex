<?php
namespace InteractiveValues\Theme\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class LineWrapViewHelper extends AbstractViewHelper
{

    /**
     * @param string $content
     * @return string
     */
    public function render($content = null)
    {
        if (empty($content)) {
            $content = (string)$this->renderChildren();
        }

        $lines = GeneralUtility::trimExplode(LF, $content, true);
        $out = [];
        foreach ($lines as $line) {
            $out[] = '<span>' . $line . '</span>';
        }

        return implode(LF, $out);
    }
}
