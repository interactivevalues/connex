<?php

namespace InteractiveValues\Theme\Hooks\Backend;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Service\FlexFormService;

/**
 * Set the header of content element to a field value of the dce configuration
 */
class LabelHook
{

    /**
     * @param array $params
     */
    public function getContentElementLabel(array &$params)
    {
        if (StringUtility::beginsWith((string)$params['row']['CType'][0], 'dce_') && !is_array($params['row']['pi_flexform'])) {
            /** @var FlexFormService $flexFormService */
            $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
            $data = $flexFormService->convertFlexFormContentToArray($params['row']['pi_flexform']);

            $params['title'] = $data['settings']['title'];
        } else {
            $params['title'] = $params['row']['header'];
        }
    }
}