<?php

namespace InteractiveValues\Theme\Hooks\Frontend;

/**
 * Hook is called before outputting
 */
class PageGenerationHook
{

    public function beforeInCache(array &$params)
    {
        $params['pObj']->content = trim($params['pObj']->content);
    }
}