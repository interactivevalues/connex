# TypoScript for rendering in frontend

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:gridelements/Configuration/TypoScript/setup.ts">

tt_content.gridelements_pi1.20.10.setup {
	# same identifier as in tsconfig
	2spaltig < lib.gridelements.defaultGridSetup
	2spaltig {
		columns {
			401 < .default
			401 {
				preCObject = CASE
				preCObject {
					key.field = flexform_columnType
					cols-6-6 = TEXT
					cols-6-6 {
						value = <div class="col-md-6">
					}

					cols-4-8 = TEXT
					cols-4-8 {
						value = <div class="col-md-4">
					}

					default = TEXT
					default {
						value = <div class="col-md-6">
					}
				}

				wrap = <div class="container">|</div>
			}

			402 < .default
			402 {
				preCObject = CASE
				preCObject {
					key.field = flexform_columnType
					cols-6-6 = TEXT
					cols-6-6 {
						value = <div class="col-md-6">
					}

					cols-4-8 = TEXT
					cols-4-8 {
						value = <div class="col-md-8">
					}

					default = TEXT
					default {
						value = <div class="col-md-6">
					}
				}

				wrap = |</div></div>
			}
		}
	}

	3spaltig < lib.gridelements.defaultGridSetup
	3spaltig {
		columns {
			401 < .default
			401 {
				preCObject = CASE
				preCObject {
					key.field = flexform_columnType
					cols-4-4-4 = TEXT
					cols-4-4-4 {
						value = <div class="col-md-4">
					}

					default = TEXT
					default {
						value = <div class="col-md-4">
					}
				}

				wrap = <div class="container">|</div>
			}

			402 < .default
			402 {
				preCObject = CASE
				preCObject {
					key.field = flexform_columnType
					cols-4-4-4 = TEXT
					cols-4-4-4 {
						value = <div class="col-md-4">
					}

					default = TEXT
					default {
						value = <div class="col-md-4">
					}
				}

				wrap = |</div>
			}

			403 < .default
			403 {
				preCObject = CASE
				preCObject {
					key.field = flexform_columnType
					cols-4-4-4 = TEXT
					cols-4-4-4 {
						value = <div class="col-md-4">
					}

					default = TEXT
					default {
						value = <div class="col-md-4">
					}
				}

				wrap = |</div></div>
			}
		}
	}

	bgGrey < lib.gridelements.defaultGridSetup
	bgGrey {
		columns {
			401 < .default
			401 {
				preCObject = CASE
				preCObject {
					key.field = flexform_columnType
					default = TEXT
					default {
						value =
					}
				}
				wrap = <div class="bgGrey full">|</div>
			}
		}
	}
}