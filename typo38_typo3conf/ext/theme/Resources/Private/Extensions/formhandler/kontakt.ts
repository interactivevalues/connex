config.admPanel=1
plugin.Tx_Formhandler.settings.debug = 0



plugin.Tx_Formhandler.settings {

  predef.formKontakt {
    name =  Kontaktformular Default AT-DE

    addErrorAnchors = 1
    #templateFile = fileadmin/dermacher/formhandler/template.html
    langFile.1 = fileadmin/cx/templates/ext/formhandler/kontakt.xml
    #cssFile.1 = fileadmin/dermacher/templates/ext/formhandler/form.css

    # Template file
    templateFile = fileadmin/cx/templates/ext/formhandler/form_simple_contact.html

    # Master template
    masterTemplateFile = fileadmin/cx/templates/ext/formhandler/master_template_contact.html

    formValuesPrefix = formhandler

    requiredSign = TEXT
    requiredSign.value = *
    requiredSign.wrap = <span style="color: #FF0000;">|</span>

    isErrorMarker.default = class="errorField"

    # HTML wrapping by validation error
    errorListTemplate {
      totalWrap = <div class="error"><ul>|</ul></div>
      singleWrap = <li>|</li>
    }

    #PreProcessors
    preProcessors {
      1.class = PreProcessor_LoadGetPost
      2.class = PreProcessor_ValidateAuthCode
      2.config {
        #redirectPage = 1
        #errorRedirectPage = 1
        hiddenField = hidden
      }
    }

    # Validators configuration
    validators {
      1 {
        class = Tx_Formhandler_Validator_Default
        config {
          fieldConf {
            #gender {
            #  errorCheck.1 = required
            #}
            firstname {
              errorCheck.1 = required
            }
            lastname {
              errorCheck.1 = required
            }
            company {
              errorCheck.1 = required
            }
            address {
              errorCheck.1 = required
            }
            zip {
              errorCheck.1 = required
            }
            city {
              errorCheck.1 = required
            }
            email {
              errorCheck.1 = required
              errorCheck.2 = email
            }
            phone {
              errorCheck.1 = required
            }
            message {
              errorCheck.1 = required
            }
          }
        }
      }
    }

    # Loggers configuration
    loggers {
      10 {
        class = Logger\DB
        config {
          disableIPlog = 1
        }
      }
    }

    # Finishers configuration
    initInterceptors {
      1 {
        class = Interceptor_CombineFields
        config {
          combineFields {
            subject {
              value = Ihre Anfrage
              fields {
                1 = firstname
                2 = lastname
              }
              separator = -
              hideEmptyValues = 1
            }
          }
        }
      }
    }

    # Finishers configuration
    finishers {

      3.class = Tx_Formhandler_Finisher_Mail
      3.config {
        admin {
          to_email = office@connexgroup.net
          bcc_email = chg@connexgroup.net
          to_name = Connex Markting Group
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Ihre Anfrage / {GP:formhandler|subject}
          sender_email = email
          #sender_name = Connex Markting Group
          htmlEmailAsAttachment = 0
        }
        user {
          to_email = email
          #bcc_email = chg@connexgroup.net
          to_name = Connex Markting Group
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Ihre Anfrage / {GP:formhandler|subject}
          sender_email = email
          #sender_name = Connex Markting Group
          htmlEmailAsAttachment = 0
        }
      }

      4.class = Finisher_Redirect
      4.config {
        #redirectPage = 1
        #redirectPage = 4
      }

      10.class = Finisher_SubmittedOK
      10.config.returns = 1
    }

    #debuggers.1 {
    #  class = TypoheadsFormhandlerDebuggerPrintToScreen
    #  config {
    #    sectionWrap = <div class=”debug-section”>|</div>
    #  }
    #}

  }



}





