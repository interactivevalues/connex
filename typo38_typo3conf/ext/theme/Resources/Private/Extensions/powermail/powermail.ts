plugin.tx_powermail {
    view {
        templateRootPaths {
            1 = EXT:theme/Resources/Private/Extensions/powermail/Templates/
        }
        partialRootPaths {
            1 = EXT:theme/Resources/Private/Extensions/powermail/Partials/
        }
    }
}