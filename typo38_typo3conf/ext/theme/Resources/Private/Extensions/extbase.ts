# ******************************************************************************
#  (c) 2012 Georg Ringer <typo3@ringerge.org>
#
#  You can redistribute it and/or modify it under the terms of the
#  GNU General Public License as published by the Free Software Foundation;
#  either version 2 of the License, or (at your option) any later version.
# ******************************************************************************


# **********************************************************
# Extbase configuration
# **********************************************************
config {
  tx_extbase {
    mvc {
      callDefaultActionIfActionCantBeResolved = 1
    }
  }
}

config.tx_extbase {
  objects {
    Tx_Extbase_Persistence_Storage_BackendInterface {
      className = Tx_Extbase_Persistence_Storage_Typo3DbBackend
    }
  }
  mvc {
    requestHandlers {
      TYPO3\CMS\Extbase\Mvc\Web\FrontendRequestHandler = TYPO3\CMS\Extbase\Mvc\Web\FrontendRequestHandler
      TYPO3\CMS\Extbase\Mvc\Web\BackendRequestHandler = TYPO3\CMS\Extbase\Mvc\Web\BackendRequestHandler
    }
  }
}