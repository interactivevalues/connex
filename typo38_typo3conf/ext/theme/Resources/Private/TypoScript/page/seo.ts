

# **********************************************************
# Seo related stuff
# **********************************************************

page.meta {
	keywords.field = keywords
	keywords.override.data = register:newsKeywords
	description.field = description
	abstract.field = abstract
}

# **********************************************************
# <title> - Tag
# **********************************************************
# Important
# Never use config.titleTagFunction
# ---------------------------------
config.pageTitleFirst = 1
config.pageTitleSeparator = -
config.pageTitleSeparator.noTrimWrap = | | |


# **********************************************************
# Various icons, must be placed in root
# **********************************************************
page {
	shortcutIcon = favicon.ico

	headerData.31 = TEXT
	headerData.31.value (
		<link rel="apple-touch-icon" sizes="57x57" href="{$plugin.theme_configuration.url}apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="60x60" href="{$plugin.theme_configuration.url}apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="{$plugin.theme_configuration.url}apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="{$plugin.theme_configuration.url}apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="{$plugin.theme_configuration.url}apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="{$plugin.theme_configuration.url}apple-touch-icon-152x152.png" />
		<link rel="apple-touch-icon" sizes="180x180" href="{$plugin.theme_configuration.url}apple-touch-icon-180x180.png" />
	 )
}

# **********************************************************
# Canonical Tag to current page
# **********************************************************
page.headerData.32 = TEXT
page.headerData.32 {
		typolink {
			parameter.data = TSFE:id
			addQueryString = 1
			addQueryString.exclude = id
			returnLast = url
		}
		noTrimWrap (
|
<link rel="canonical" href="|" />
|
)
}

# **********************************************************
# Alternative language tag, linking to translated pages
# **********************************************************
#includeLibs.alternativepages = EXT:theme/Classes/Page/AlternativeLanguageTag.php
#page.headerData.33 = USER
#page.headerData.33 {
#	userFunc = Cyberhouse\Theme\Page\AlternativeLanguageTag->get

	# Use either one of those, see below for its configuration
#	useRealurl = 1
#	useSimpleMode = 0

	# Use the realurl mapping
#	realurl {
#		key = _DEFAULT
#		default = de
#	}

	# Use the simple mapping
#	simpleMode {
		#0 = en
		#1 = de
		#2 = fr
		#3 = it
#	}
#}

# **********************************************************
# Google Analytics
# **********************************************************
#page.headerData.919 = COA
#page.headerData.919 {

#	stdWrap.wrap = <script type="text/javascript">|</script>
#	stdWrap.if.value = {$plugin.theme_configuration.general.googleanalytics}
#	stdWrap.if.equals = 1

#	10 = TEXT
#	10.value = (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new #Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

#	20 = TEXT
#	20.value = {$plugin.theme_configuration.general.googleanalytics.code}
#	20.wrap = ga('create','|','auto');ga('send','pageview')
#}
