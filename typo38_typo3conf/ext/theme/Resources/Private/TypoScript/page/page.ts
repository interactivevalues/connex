# **********************************************************
# General PAGE setup
#
# including template, CSS + JS files
# **********************************************************

page = PAGE

[treeLevel = 0]
page.config.noPageTitle = 1
page.headerData.10 = TEXT
page.headerData.10.field = subtitle // title
page.headerData.10.wrap = <title>|</title>
[global]

# be sure to display website correctly in mobile browsers
page.headerData.10 = TEXT
page.headerData.10.value (
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <link rel="apple-touch-icon" sizes="180x180" href="fileadmin/cx/templates/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="fileadmin/cx/templates/images/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="fileadmin/cx/templates/images/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="fileadmin/cx/templates/images/favicon/manifest.json">
  <link rel="mask-icon" href="fileadmin/cx/templates/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="fileadmin/cx/templates/images/favicon/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">

)
page {
	shortcutIcon = favicon.ico
	# Page template
	10 = FLUIDTEMPLATE
	10 {
		file.stdWrap.cObject = TEXT
		file.stdWrap.cObject {
			data = pagelayout
			split {
				token = pagets__
				1.current = 1
				1.wrap = |
			}

			ifEmpty = home
			wrap = EXT:theme/Resources/Private/Templates/Page/|.html
		}

		layoutRootPath = EXT:theme/Resources/Private/Templates/Page/Layouts/
		variables {

		}
	}

	# CSS files to be included
	includeCSS {
		bootstrap = fileadmin/cx/templates/css/bootstrap.css
		slick = fileadmin/cx/templates/css/slick.css
		slick_theme = fileadmin/cx/templates/css/slick-theme.css
		styles = fileadmin/cx/templates/css/style.css
		styles_cx = fileadmin/cx/templates/css/style_cx.css
		stylesAddon = fileadmin/cx/templates/css/style_cx_addon.css
		stylesPraemien = {$plugin.theme_configuration.styles}
	}

	includeJS {
		jquery = fileadmin/cx/templates/js/jquery.js
		bootstrap = fileadmin/cx/templates/js/bootstrap.js
		easing = http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js
		easing.external = 1
		classie = fileadmin/cx/templates/js/classie.js
		masonry = fileadmin/cx/templates/js/masonry.pkgd.min.js
		dev = fileadmin/cx/templates/js/dev.js
		appear = fileadmin/cx/templates/js/appear.js
		slide = fileadmin/cx/templates/js/content_slide.js
		agency = fileadmin/reisecenter/templates/js/agency.js
	}

	bodyTagCObject = COA
	bodyTagCObject {
		stdWrap.wrap = <body class="|">

		# Add page UID
		10 = TEXT
		10 {
			value = page-{field:uid}
			insertData = 1
			noTrimWrap = || |
		}

		# Add current language
		20 = TEXT
		20 {
			value = language-{TSFE:sys_language_uid} languagecontent-{TSFE:sys_language_content}
			insertData = 1
			noTrimWrap = || |
		}

		# Add level
		25 = TEXT
		25 {
			value = level-{level:0}
			insertData = 1
			noTrimWrap = || |
		}

		# Add backend-layout
		30 = TEXT
		30 {
			wrap = |
			data = pagelayout
			split {
				token = pagets__
				1.current = 1
				1.wrap = |
			}
		}

		# Add uid of optional FE-layout
		40 = TEXT
		40 {
			fieldRequired = layout
			value = layout-{field:layout}
			insertData = 1
			noTrimWrap = | ||
		}
	}
}

### css & js module
###########################################################
page.45 = TEXT
page.45.value (

    <!-- Custom Theme JavaScript -->
    <script src="http://www.connexgroup.net/fileadmin/reisecenter/templates/js/slick.js"></script>


    <script type="text/javascript">
    // create the back to top button
    $('body').prepend('<a href="#" class="back-to-top"><i class="fa fa-angle-double-up" aria-hidden="true"></i> nach oben</a>');

    var amountScrolled = 300;

    $(window).scroll(function() {
      if ( $(window).scrollTop() > amountScrolled ) {
        $('a.back-to-top').fadeIn('slow');
      } else {
      $('a.back-to-top').fadeOut('slow');
      }
    });

    $('a.back-to-top, a.simple-back-to-top').click(function() {
      $('html, body').animate({
        scrollTop: 0
      }, 700);
      return false;
    });
    </script>

    <script>
      /*
      $('#myCarousel').carousel({
        interval:   4000
      });
      */
    </script>


  <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/cookiechoices.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function(event) {
      cookieChoices.showCookieConsentBar('Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden.',
        'Ok', 'Weitere Informationen', '');
    });
  </script>

)




# **********************************************************
# Issue Collector, e.g. JIRA
# **********************************************************
#[globalVar = TSFE : beUserLogin > 0]
#	page.includeJS.issueCollector = {$plugin.theme_configuration.general.issueCollectorJsPath}
#	page.includeJS.issueCollector {
#		excludeFromConcatenation = 1
#		external = 1
#		disableCompression = 1
#	}
#[global]

[browser = msie] && [version < 9]
page.includeCSS{
	html5 = EXT:theme/Resources/Public/Templates/scripts/html5.js
}
[global]
