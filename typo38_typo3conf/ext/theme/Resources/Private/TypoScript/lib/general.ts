
# **********************************************************
# Library for TypoScript about general library objects.
# **********************************************************

#-------------------------------------------------------------------------------
#	GENERAL: Copyright Information
#-------------------------------------------------------------------------------
lib.copyright_information = COA
lib.copyright_information {
	10 = TEXT
	10 {
		data = date:U
		strftime = %Y
		noTrimWrap = || {LLL:EXT:theme/Resources/Private/Language/locallang.xml:general.copyright}|
		noTrimWrap.insertData = 1
		typolink.parameter = {$plugin.theme_configuration.general.copyright_information.link}
	}
}

#-------------------------------------------------------------------------------
#	GENERAL: Logo
#-------------------------------------------------------------------------------
lib.general.logo = COA
lib.general.logo {
	10 = IMAGE
	10 {
		#ATagParams = class="hasRetina"
		file = EXT:theme/Resources/Public/Template/images/BuH-Logo.png
		#params = class="hasRetina"
		stdWrap.typolink.parameter = {$plugin.theme_configuration.url}
		altText = Logo B&H - zur Startseite
		titleText = Logo B&H - zur Startseite
	}
}

#-------------------------------------------------------------------------------
#	GENERAL: Link to search page
#-------------------------------------------------------------------------------
lib.general.searchPageLink = TEXT
lib.general.searchPageLink {
	typolink {
		parameter =  {$plugin.theme_configuration.navigation.searchPage}
		returnLast = url
	}
}

#-------------------------------------------------------------------------------
#	GENERAL: Header image
#-------------------------------------------------------------------------------
lib.general.headerImage = IMG_RESOURCE
lib.general.headerImage {
	file {
		import =  uploads/media/
		import.data = levelmedia:-1, slide
		treatIdAsReference = 1
		import.listNum = 0
	}
}

#-------------------------------------------------------------------------------
#	GENERAL: CURRENT URL
#-------------------------------------------------------------------------------
lib.general.currentURL= TEXT
lib.general.currentURL.data = getIndpEnv:TYPO3_REQUEST_URL

#-------------------------------------------------------------------------------
#  GENERAL: aktueller Seitentitel
#-------------------------------------------------------------------------------
#lib.general.sitetitle = TEXT
#lib.general.sitetitle.data = leveltitle : 1
lib.general.sitetitle = TEXT
lib.general.sitetitle.value = {page:nav_title//page:title}
lib.general.sitetitle.insertData = 1

#-------------------------------------------------------------------------------
#  GENERAL: OG:Image
#-------------------------------------------------------------------------------
lib.general.OpenGraphImage = IMG_RESOURCE
lib.general.OpenGraphImage {
	file {
		import.data = levelmedia:-1, slide
		treatIdAsReference = 1
		import.listNum = 0
	}
}