# **********************************************************
# Rendering of all content columns
# **********************************************************

#-------------------------------------------------------------------------------
#    CONTENT: Main Content (colPos = 0)
#-------------------------------------------------------------------------------
lib.content.main = COA
lib.content.main {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
}

lib.content.0 < lib.content.main

#-------------------------------------------------------------------------------
#    CONTENT: Left Content (colPos = 1)
#-------------------------------------------------------------------------------
lib.content.left = COA
lib.content.left {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
	10.select.where = colPos=1
}

lib.content.1 < lib.content.left

#-------------------------------------------------------------------------------
#    CONTENT: Right Content (colPos = 2)
#-------------------------------------------------------------------------------
lib.content.right = COA
lib.content.right {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
	10.select.where = colPos=2
}

lib.content.2 < lib.content.right

#-------------------------------------------------------------------------------
#    CONTENT: Border Content (colPos = 3)
#-------------------------------------------------------------------------------
lib.content.border = COA
lib.content.border {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
	10.select.where = colPos=3
}

lib.content.3 < lib.content.border

#-------------------------------------------------------------------------------
#    CONTENT: colPos = 4
#-------------------------------------------------------------------------------
lib.content.4 = COA
lib.content.4 {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
	10.select.where = colPos=4
}

#-------------------------------------------------------------------------------
#    CONTENT: colPos = 5
#-------------------------------------------------------------------------------
lib.content.5 = COA
lib.content.5 {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
	10.select.where = colPos=5
}

#-------------------------------------------------------------------------------
#    CONTENT: colPos = 6
#-------------------------------------------------------------------------------
lib.content.6 = COA
lib.content.6 {
	stdWrap.innerWrap = <!--TYPO3SEARCH_begin-->|<!--TYPO3SEARCH_end-->
	10 < styles.content.get
	10.select.where = colPos=6
}

#-------------------------------------------------------------------------------
#    CONTENT: colPos = 99
#-------------------------------------------------------------------------------
lib.content.json = COA
lib.content.json {
	10 < styles.content.get
	10.select.where = colPos=99
}

