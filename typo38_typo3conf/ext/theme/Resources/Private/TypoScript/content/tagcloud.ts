#-------------------------------------------------------------------------------
#  GENERAL: Tagcloud
#-------------------------------------------------------------------------------
lib.general.tagcloud = COA
lib.general.tagcloud{
    10 = CONTENT
    10{
        table = tt_content
        select.pidInList = 19
        select.where = colPos = 2
        select.max = 1
        #select.orderBy = rand()
    }
}