### header logo / navigation
###########################################################
lib.general.headerNav_VORTEIL = COA
lib.general.headerNav_VORTEIL {
    10 = COA
    10 {
        # mobile icon
        10 = COA
        10 {
            10 = COA
            10 {
                10 = TEXT
                10.value = <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            }
            10.wrap = <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">|</button>

            20 = COA
            20 {
                10 = COA
                10 {
                    10 = TEXT
                    10.value = <a class="navbar-brand" href="http://www.connexgroup.net/"><img src="http://www.connexgroup.net/fileadmin/cx/templates/images/svg_icons/Connex_Marketing_Group_Logo_2016_4c.svg"></a>
                }
            }
            20.wrap = <div class="col-xs-12 header_logo">|</div>
        }
        10.wrap = <div class="navbar-header page-scroll">|</div>

        # navigation
        20 = COA
        20 {
            10 = COA
            10 {
                10 = TEXT
                10.value = <li class="hidden"><a href="#page-top"></a></li>

                20 = >
                20 = TEXT
                20.value = <li class=""><a class="page-scroll" href="#refinanzierung">Refinanzierung</a></li>

                30 = >
                30 = TEXT
                30.value = <li class="hidden-sm"><a class="page-scroll" href="#konsumenten">Ziele</a></li>

                40 = >
                40 = TEXT
                40.value = <li class=""><a class="page-scroll" href="#zielgruppe">Für wen?</a></li>

                60 = >
                60 = TEXT
                60.value = <li class=""><a class="page-scroll" href="#erfolgsbeispiele">Erfolgsbeispiele</a></li>

                70 = >
                70 = TEXT
                70.value = <li class=""><a class="page-scroll" href="#contact">Kontakt</a></li>

            }
            10.wrap = <ul class="nav navbar-nav navbar-right">|</ul>
        }
        20.wrap = <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">|</div>
    }
    10.wrap = <nav class="navbar navbar-default navbar-fixed-top"><div class="container">|</div></nav>
}

lib.content.headerVorteil = COA
lib.content.headerVorteil{
    10 = CONTENT
    10{
        table = tt_content
        select {
            pidInList = 411
            orderBy = sorting
            where = colPos=0
            #languageField = sys_language_uid
        }
    }
}

lib.content.vorteil_01 < lib.content.headerVorteil
lib.content.vorteil_01{
    10.select.pidInList = 415
}

lib.content.vorteil_02 < lib.content.headerVorteil
lib.content.vorteil_02{
    10.select.pidInList = 417
}

lib.content.vorteil_03 < lib.content.headerVorteil
lib.content.vorteil_03{
    10.select.pidInList = 419
}

lib.content.vorteil_04 < lib.content.headerVorteil
lib.content.vorteil_04{
    10.select.pidInList = 421
}

lib.content.vorteil_05 < lib.content.headerVorteil
lib.content.vorteil_05{
    10.select.pidInList = 423
}

lib.content.vorteil_06 < lib.content.headerVorteil
lib.content.vorteil_06{
    10.select.pidInList = 425
}

lib.content.vorteil_07 < lib.content.headerVorteil
lib.content.vorteil_07 {
    10.select.pidInList = 427
}

lib.content.vorteil_08 < lib.content.headerVorteil
lib.content.vorteil_08 {
    10.select.pidInList = 413
}

lib.content.vorteil_09 < lib.content.headerVorteil
lib.content.vorteil_09 {
    10.select.pidInList = 429
}

### contact ###
lib.content.contactVorteil = COA
lib.content.contactVorteil {
    10 = COA
    10 {
        8 = COA
        8 {
        }
        8.wrap = <div class="cx-triangle"></div>

        10 = COA
        10 {
            10 = RECORDS
            10.tables = tt_content
            10.source = 533
        }
        10.wrap = <div class="contact text-center">|</div>

        20 = TEXT
        20.value = <p>Füllen Sie dieses Formular aus, wenn Sie weitere Informationen zu den Connex Lösungen oder ein unverbindliches Beratungsgespräch wünschen.</p>
        20.value.wrap = <div class="contact-teaser text-center">|</div>

        # container contact address
        30 = COA
        30 {
            # contact content
            10 = COA
            10 {
                10 = RECORDS
                10.tables = tt_content
                10.source = 531
            }
            10.wrap = <div class="col-md-4 col-sm-5 col-xs-12 contact-left">|</div>
            # powermail
            20 = COA
            20 {
                10 = RECORDS
                10.tables = tt_content
                10.source = 825
            }
            20.wrap = <div class="col-md-8 col-sm-7 col-xs-12 contact-form">|</div>
            # footer
            30 = COA
            30 {

            }
            30.wrap = <div class="footer-block">|</div>
        }
        30.wrap = <div class="col-xs-12 no-padding-0 contact-block">|</div>
    }
    10.wrap = <div class="container ">|</div>

    20 = COA
    20 {
        10 = COA
        10 {
            10 = TEXT
            10.value = <li><a href="http://www.connexgroup.net">Home</a></li>
            20 = TEXT
            20.value = <li><a class="page_scroll" href="#contact">Kontakt</a></li>
            30 = TEXT
            30.value = <li><a href="" data-toggle="modal" data-target="#myModalImpressum">Impressum</a></li>
        }
        10.wrap =
    }
    20.wrap = <div class="container "><div class="col-xs-12 no-padding-0 footer-block"><ul>|</ul></div></div>
}

### overlay impressum
###########################################################
lib.modalImpressumHeader = COA
lib.modalImpressumHeader {
    10 = COA
    10 {
        10 = TEXT
        10.value = <h4 class="modal-title" id="myModalLabelImpressum">Impressum</h4>
    }
}

lib.modalImpressumContent = COA
lib.modalImpressumContent {
    10 = COA
    10 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 163

        20 = RECORDS
        20.tables = tt_content
        20.source = 165
    }
}