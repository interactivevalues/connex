# **********************************************************
# Library for TypoScript about navigations.
# **********************************************************
### header logo / navigation
###########################################################
lib.general.headerNav_PRAEMIEN = COA
lib.general.headerNav_PRAEMIEN {
    10 = COA
    10 {
        # mobile icon
        10 = COA
        10 {
            10 = COA
            10 {
                10 = TEXT
                10.value = <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            }
            10.wrap = <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">|</button>

            20 = COA
            20 {
                10 = COA
                10 {
                    10 = TEXT
                    10.value = <a class="navbar-brand" href="http://www.connexgroup.net/"><img src="http://www.connexgroup.net/fileadmin/cx/templates/images/svg_icons/Connex_Marketing_Group_Logo_2016_4c.svg"></a>
                }
            }
            20.wrap = <div class="col-xs-12 header_logo">|</div>
        }
        10.wrap = <div class="navbar-header page-scroll">|</div>

        # navigation
        20 = COA
        20 {
            10 = COA
            10 {
                10 = TEXT
                10.value = <li class="hidden"><a href="#page-top"></a></li>

                20 = >
                20 = TEXT
                20.value = <li class=""><a class="page-scroll" href="#sales_increase">Komplettlösung</a></li>

                30 = >
                30 = TEXT
                30.value = <li class=""><a class="page-scroll" href="#one_to_one">One-To-One-Kommunikation</a></li>

                40 = >
                40 = TEXT
                40.value = <li class=""><a class="page-scroll" href="#case_studies">Erfolgsbeispiele</a></li>

                50 = >
                50 = TEXT
                50.value = <li class=""><a class="page-scroll" href="#whitepaper">Whitepaper</a></li>

                90 = >
                90 = TEXT
                90.value = <li class=""><a class="page-scroll" href="#contact">Kontakt</a></li>

            }
            10.wrap = <ul class="nav navbar-nav navbar-right">|</ul>
        }
        20.wrap = <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">|</div>
    }
    10.wrap = <nav class="navbar navbar-default navbar-fixed-top"><div class="container">|</div></nav>
}

lib.content.header = COA
lib.content.header{
    10 = CONTENT
    10{
        table = tt_content
        select {
            pidInList = 245
            orderBy = sorting
            where = colPos=0
            #languageField = sys_language_uid
        }
    }
}

lib.content.sales_increase < lib.content.header
lib.content.sales_increase{
    10.select.pidInList = 111
}

lib.content.advantages < lib.content.header
lib.content.advantages{
    10.select.pidInList = 241
}

lib.content.business_insights < lib.content.header
lib.content.business_insights{
    10.select.pidInList = 243
}

lib.content.one_to_one < lib.content.header
lib.content.one_to_one{
    10.select.pidInList = 247
}

lib.content.service < lib.content.header
lib.content.service{
    10.select.pidInList = 249
}

lib.content.box_modal_infos < lib.content.header
lib.content.box_modal_infos{
    10.select.pidInList = 257
}

lib.content.case_studies < lib.content.header
lib.content.case_studies{
    10.select.pidInList = 251
}

lib.content.whitepaper < lib.content.header
lib.content.whitepaper{
    10.select.pidInList = 255
}


### contact ###
lib.content.contact = COA
lib.content.contact {
    10 = COA
    10 {
        8 = COA
        8 {
        }
        8.wrap = <div class="cx-triangle"></div>

        10 = COA
        10 {
            10 = RECORDS
            10.tables = tt_content
            10.source = 533
        }
        10.wrap = <div class="contact text-center">|</div>

        20 = TEXT
        20.value = <p>Füllen Sie dieses Formular aus, wenn Sie weitere Informationen zu den Connex Lösungen oder ein unverbindliches Beratungsgespräch wünschen.</p>
        20.value.wrap = <div class="contact-teaser text-center">|</div>

        # container contact address
        30 = COA
        30 {
            # contact content
            10 = COA
            10 {
                10 = RECORDS
                10.tables = tt_content
                10.source = 531
            }
            10.wrap = <div class="col-md-4 col-sm-5 col-xs-12 contact-left">|</div>
            # powermail
            20 = COA
            20 {
                10 = RECORDS
                10.tables = tt_content
                10.source = 711
            }
            20.wrap = <div class="col-md-8 col-sm-7 col-xs-12 contact-form">|</div>
            # footer
            30 = COA
            30 {

            }
            30.wrap = <div class="footer-block">|</div>
        }
        30.wrap = <div class="col-xs-12 no-padding-0 contact-block">|</div>
    }
    10.wrap = <div class="container ">|</div>

    20 = COA
    20 {
        10 = COA
        10 {
            10 = TEXT
            10.value = <li><a href="http://www.connexgroup.net">Home</a></li>
            20 = TEXT
            20.value = <li><a class="page_scroll" href="#contact">Kontakt</a></li>
            30 = TEXT
            30.value = <li><a href="" data-toggle="modal" data-target="#myModalImpressum">Impressum</a></li>
        }
        10.wrap =
    }
    20.wrap = <div class="container "><div class="col-xs-12 no-padding-0 footer-block"><ul>|</ul></div></div>
}

### overlay impressum
###########################################################
lib.modalImpressumHeader = COA
lib.modalImpressumHeader {
    10 = COA
    10 {
        10 = TEXT
        10.value = <h4 class="modal-title" id="myModalLabelImpressum">Impressum</h4>
    }
}

lib.modalImpressumContent = COA
lib.modalImpressumContent {
    10 = COA
    10 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 163

        20 = RECORDS
        20.tables = tt_content
        20.source = 165
    }
}