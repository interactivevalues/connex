#-------------------------------------------------------------------------------
#    Various extensions
#-------------------------------------------------------------------------------
#<INCLUDE_TYPOSCRIPT: source="DIR:EXT:theme/Resources/Private/Extensions/gridelements/" extensions="ts2">
#<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/PageTSconfig/NewContentElementWizard.ts">

#-------------------------------------------------------------------------------
#    RTE
#-------------------------------------------------------------------------------
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:theme/Resources/Private/TsConfig/Page/rte/default.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:theme/Resources/Private/TsConfig/Page/layouts.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:theme/Resources/Private/TsConfig/Page/gridelements.ts">
tx_news.module.preselect {
	sortingField = datetime
	sortingDirection = desc
}

