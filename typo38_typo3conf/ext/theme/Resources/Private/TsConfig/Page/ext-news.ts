#-------------------------------------------------------------------------------
#    EXT:news
#-------------------------------------------------------------------------------
tx_news.templateLayouts {
	# 1 = Dummy
}

TCEFORM {
	tx_news_domain_model_news {
		editlock.disabled = 1
		alternative_title.disabled = 1
		path_segment.disabled = 1
	}
}