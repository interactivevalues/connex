
# **********************************************************
# Constants
# **********************************************************
lib.constants.permissions.groupid = 1

#-------------------------------------------------------------------------------
#    Cleanup fields
#-------------------------------------------------------------------------------
TCEFORM {
	pages {
		alias.disabled = 1
		author.disabled = 1
		author_email.disabled = 1
		categories.disabled = 1
		cache_tags.disabled = 1
		doktype.removeItems = 6
		editlock.disabled = 1
		lastUpdated.disabled = 1
		newUntil.disabled = 1
		no_cache.disabled = 1
		content_from_pid.disabled = 1
		php_tree_stop.disabled = 1
		storage_pid.disabled = 1
		url_scheme.removeItems = 0

		# Backend Layouts
		backend_layout.PAGE_TSCONFIG_ID >
		backend_layout_next_level.PAGE_TSCONFIG_ID >

		# Hide no backend layout label, deactivated by default!
		#backend_layout.removeItems = 0,-1
		#backend_layout_next_level.removeItems = -1
	}

	pages_language_overlay {
		alias.disabled = 1
		author.disabled = 1
		author_email.disabled = 1
	}

	tt_content {
		CType.removeItems = bullets,multimedia,media,html,div,mailform
		date.disabled = 1
		header_position.disabled = 1
		header_link.disabled = 1
		image_noRows.disabled = 1
		imagecols.removeItems = 5,7,8
		imagecaption_position.disabled = 1
		image_compression.disabled = 1
		image_effects.disabled = 1
		imageborder.disabled = 1
		linkToTop.disabled = 1
		layout.disabled = 1
		section_frame.disabled = 1
		sectionIndex.disabled = 1
		rte_enabled.disabled = 1
		spaceBefore.disabled = 1
		spaceAfter.disabled = 1

		header_layout.altLabels {
			1 = LLL:EXT:theme/Resources/Private/Language/locallang_be.xml:tt_content.header_layout.1
			2 = LLL:EXT:theme/Resources/Private/Language/locallang_be.xml:tt_content.header_layout.2
			3 = LLL:EXT:theme/Resources/Private/Language/locallang_be.xml:tt_content.header_layout.3
			4 = LLL:EXT:theme/Resources/Private/Language/locallang_be.xml:tt_content.header_layout.4
			5 = LLL:EXT:theme/Resources/Private/Language/locallang_be.xml:tt_content.header_layout.5
		}
	}


}

#-------------------------------------------------------------------------------
#    Page module
#-------------------------------------------------------------------------------
# Set the default label and flag
mod {
	# Default flag
	SHARED {
		defaultLanguageLabel = German
		defaultLanguageFlag = de.gif
	}
}

#-------------------------------------------------------------------------------
# Defaults for new records
#-------------------------------------------------------------------------------
TCAdefaults {
	tt_content {
		header_layout = 2
	}
}

#-------------------------------------------------------------------------------
# Special backend condition to set defaults for elements created
# in non main column
#-------------------------------------------------------------------------------
#[globalVar = GP:defVals|tt_content|colPos = 1|2|3|4|5]
#TCAdefaults.tt_content {
#    header_layout = 3
#}
#[end]


#-------------------------------------------------------------------------------
#    Permissions
#-------------------------------------------------------------------------------
TCEMAIN.permissions {
	# owner
	#userid = 1
	# group
	groupid < lib.constants.permissions.groupid
	# all rights for group
	group = 31
}

# Remove 'copy X' text
TCEMAIN.table {
	pages.disablePrependAtCopy = 1
	tt_content.disablePrependAtCopy = 1
}

TCAdefaults.tx_news_domain_model_news.hidden=1