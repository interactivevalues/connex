#-------------------------------------------------------------------------------
#    Backend Layouts
#-------------------------------------------------------------------------------
mod.web_layout.BackendLayouts {
	#######################
	onepage_praemien {
		title = PRÄMIENPROGRAMME
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
	onepage_ideen {
		title = IDEENMANAGEMENT
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
	onepage_promo {
		title = PROMOTIONSYSTEME
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
	onepage_empfehlung {
		title = EMPFEHLUNGSSYSTEME
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
	onepage_daten {
		title = DATENGENERIERUNG
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
	onepage_gutschein {
		title = GUTSCHEINLÖSUNGEN
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
	onepage_vorteil {
		title = VORTEILSPROGRAMME
		icon = EXT:theme/Resources/Public/Backend/Layouts/1col.png
		config.backend_layout {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = Inhaltsbereich
							colPos = 0
						}
					}
				}
			}
		}
	}
}