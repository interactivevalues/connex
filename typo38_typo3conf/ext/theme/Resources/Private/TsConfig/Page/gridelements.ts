# Page TSConfig:
tx_gridelements.setup {
    # Identifier
    2spaltig {
        title = 2 Spalten
        config {
            colCount = 2
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Links
                            colPos = 401
                        }
                        2 {
                            name = Rechts
                            colPos = 402
                        }
                    }
                }
            }
        }
        flexformDS = FILE:typo3conf/ext/theme/Resources/Private/Extensions/gridelements/gridelements_2col_flexform.xml
    }

    3spaltig {
        title = 3 Spalten
        config {
            colCount = 3
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Links
                            colPos = 401
                        }
                        2 {
                            name = Mitte
                            colPos = 402
                        }
                        3 {
                            name = Rechts
                            colPos = 403
                        }
                    }
                }
            }
        }
        flexformDS = FILE:typo3conf/ext/theme/Resources/Private/Extensions/gridelements/gridelements_3col_flexform.xml
    }


}