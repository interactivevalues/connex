<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// Add TsConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/Page/_main.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/User/main.ts">');

// Add menu item to clear system cache for Development & Testing context
$context = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->__toString();
if ($context === 'Development' || $context === 'Testing') {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('options.clearCache.system = 1');
}

// Modify flexform values
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_befunc.php']['getFlexFormDSClass'][$_EXTKEY] =
    'InteractiveValues\\Theme\\Hooks\\Backend\\BackendUtilityHook';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all']['theme']
 = 'InteractiveValues\\Theme\\Hooks\\Frontend\\PageGenerationHook->beforeInCache';


$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['news_json'] = 'EXT:theme/Classes/Controller/JsonController.php';
/*
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['tx_realurl_tcemain'] = array(
 'className' => 'InteractiveValues\\Theme\\Xclass\\RealurlHook'
 );
*/