<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/***************
 * Embed TypoScript
 */
$contexts = array('development', 'testing', 'production', 'custom');
foreach ($contexts as $context) {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
		$_EXTKEY,
		'Configuration/TypoScript/' . ucfirst($context), 'Theme EXT:"' . $_EXTKEY . '" ' . strtoupper($context)
	);
}

/***************
 * Include styling for backend/login
 */
$context = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->__toString();
if (!empty($context)) {
	$GLOBALS['TBE_STYLES']['logo'] = '../typo3conf/ext/' . $_EXTKEY . '/Resources/Public/Backend/Styles/img/backend_logo_' . strtolower($context) . '.png';
} else {
	$GLOBALS['TBE_STYLES']['logo'] = '../typo3conf/ext/' . $_EXTKEY . '/Resources/Public/Backend/Styles/img/backend_logo.png';
}
