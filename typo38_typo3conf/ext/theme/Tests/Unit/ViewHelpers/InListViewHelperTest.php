<?php

namespace InteractiveValues\Theme\Tests\Unit\ViewHelpers;

use InteractiveValues\Theme\ViewHelpers\InListViewHelper;

class InListViewHelperTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function inListRendersThenChild() {
		$viewHelper = $this->getMock(InListViewHelper::class, array('renderThenChild'));
		$viewHelper->expects($this->once())->method('renderThenChild');

		$viewHelper->render('123,456,789', '123');
	}

	/**
	 * @test
	 */
	public function inListRendersElseChild() {
		$viewHelper = $this->getMock(InListViewHelper::class, array('renderElseChild'));
		$viewHelper->expects($this->once())->method('renderElseChild');

		$viewHelper->render('123,456,789', '1234');
	}
}