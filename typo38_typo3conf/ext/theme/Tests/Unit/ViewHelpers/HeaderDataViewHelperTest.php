<?php

namespace InteractiveValues\Theme\Tests\Unit\ViewHelpers;

class HeaderDataViewHelperTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject|\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController|\TYPO3\CMS\Core\Tests\AccessibleObjectInterface
	 */
	protected $tsfe = NULL;

	public function setUp() {
		$this->tsfe = $this->getAccessibleMock(\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController::class, array('dummy'), array(), '', FALSE);
		$GLOBALS['TSFE'] = $this->tsfe;
	}

	/**
	 * @test
	 */
	public function headerDataCanBeSet() {
		$title = 'Some header content';
		$viewHelper = $this->getMock(\InteractiveValues\Theme\ViewHelpers\HeaderDataViewHelper::class, array('renderChildren'));
		$viewHelper->expects($this->once())->method('renderChildren')->will($this->returnValue($title));

		$viewHelper->render();
		$this->assertEquals($title, $GLOBALS['TSFE']->additionalHeaderData[md5($title)] = $title);
	}
}