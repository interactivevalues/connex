
# Production environment is still loaded but overlayed with the changes inside production

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:theme/Configuration/TypoScript/Production/constants.txt">
# *******************************************************************
# Constants of the DEVELOPMENT mode
# !!! add everything to the production.ts and only those things
#     which differ to this section!
# *******************************************************************

plugin.theme_configuration {
  url = http://bhpp.interactivevalues-kunden.at/

  general {
    googleanalytics = 0
    pageTitle.prefix = !!DEV!! theme -
  }

  site{
    language = de
    locale = de_AT
    htmlTagLanguageKey = de
  }

  extensions {
    realurl = 1
  }

  navigation{
    top = 7
    searchPage = 34
  }
}

plugin.tx_indexedsearch {
  view {
    templateRootPath = EXT:theme/Resources/Private/Extensions/indexed_search/Templates/
    partialRootPath = EXT:theme/Resources/Private/Extensions/indexed_search/Partials/
  }
}