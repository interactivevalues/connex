<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Remove copy text
$GLOBALS['TCA']['tt_content']['columns']['header']['l10n_mode'] = '';
$GLOBALS['TCA']['tt_content']['columns']['bodytext']['l10n_mode'] = '';
$GLOBALS['TCA']['tt_content']['ctrl']['formattedLabel_userFunc'] = \InteractiveValues\Theme\Hooks\Backend\LabelHook::class . '->getContentElementLabel';