<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/***************
 * Add icons to the page tree
 */
$availableIcons = array('system', 'menufolder');
foreach ($availableIcons as $icon) {
	\TYPO3\CMS\Backend\Sprite\SpriteManager::addTcaTypeIcon(
		'pages',
		'contains-' . $icon,
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('theme') . 'Resources/Public/Backend/Icons/PageTree/' . $icon . '.png');
	$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = array(
		0 => 'LLL:EXT:theme/Resources/Private/Language/locallang_be.xml:pagetree.' . $icon,
		1 => $icon,
	);
}