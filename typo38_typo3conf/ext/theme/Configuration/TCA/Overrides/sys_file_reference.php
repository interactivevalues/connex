<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['sys_file_reference']['columns']['crop']['config']['ratios'] = array(
    '1.394' => 'Team (329x236)',
    '1.295' => 'Testimonial (123x95)',
    '1.51' => 'Service-Hintergrundbild (350x232)',
    '3.765' => 'Headerbild / Headerslider (1600x425)',
    'NaN' => 'Free',
);