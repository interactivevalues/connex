<?php

/**
* dix_UrlTool default realurl configuration
*/

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = array ( 
	'_DEFAULT' => array ( 
		'init' => array ( 
			'enableCHashCache' => '1',
			 'appendMissingSlash' => 'ifNotFile',
			 'enableUrlDecodeCache' => '1',
			 'enableUrlEncodeCache' => '1',
			 ),
		'redirects' => array (
		),
		'preVars' => array ( 
			'0' => array ( 
				'GETvar' => 'no_cache',
				'valueMap' => array ( 
					'nc' => '1',
				),
				'noMatch' => 'bypass'
			),
			'1' => array ( 
				'GETvar' => 'L',
				 'valueMap' => array ( 
					'de' => '0',
					 'en' => '1',
					 ),
				 'noMatch' => 'bypass',
				 ),
			 '2' => array ( 
				'GETvar' => 'lang',
				 'valueMap' => array ( 
					'de' => 'de',
					 'en' => 'en',
					 ),
				 'noMatch' => 'bypass',
				 ),
			 ),
		 'pagePath' => array ( 
			'type' => 'user',
			 'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
			 'spaceCharacter' => '-',
			 'languageGetVar' => 'L',
			 'expireDays' => '7',
			 'rootpage_id' => '1',
			 ),
		 'fixedPostVars' => array (
			),
		 'postVarSets' => array ( 
			'_DEFAULT' => array ( 
				// EXT:tt_news start
				'archive' => array ( 
					'0' => array ( 
						'GETvar' => 'tx_ttnews[year]',
						 ),
					 '1' => array ( 
						'GETvar' => 'tx_ttnews[month]',
						 'valueMap' => array ( 
							'january' => '01',
							 'february' => '02',
							 'march' => '03',
							 'april' => '04',
							 'may' => '05',
							 'june' => '06',
							 'july' => '07',
							 'august' => '08',
							 'september' => '09',
							 'october' => '10',
							 'november' => '11',
							 'december' => '12',
							 ),
						 ),
					 ),
				 'browse' => array ( 
					'0' => array ( 
						'GETvar' => 'tx_ttnews[pointer]',
						 ),
					 ),
				 'select_category' => array ( 
					'0' => array ( 
						'GETvar' => 'tx_ttnews[cat]',
						 ),
					 ),
				 'article' => array ( 
					'0' => array ( 
						'GETvar' => 'tx_ttnews[tt_news]',
						 'lookUpTable' => array ( 
							'table' => 'tt_news',
							 'id_field' => 'uid',
							 'alias_field' => 'title',
							 'addWhereClause' => ' AND NOT deleted',
							 'useUniqueCache' => '1',
							 'useUniqueCache_conf' => array ( 
								'strtolower' => '1',
								 'spaceCharacter' => '-',
								 ),
							 ),
						 ),
					 '1' => array ( 
						'GETvar' => 'tx_ttnews[swords]',
						 ),
					 ),
				 // EXT:tt_news end
				// EXT:news start
				'newsarticle' => array( 
					// action and controller can be omitted when news is properly configured. see documentation of the news extension.
					array( 
						'GETvar' => 'tx_news_pi1[action]',
						 ),
					 array( 
						'GETvar' => 'tx_news_pi1[controller]',
						 ),
					 array( 
						'GETvar' => 'tx_news_pi1[news]',
						 'lookUpTable' => array( 
							'table' => 'tx_news_domain_model_news',
							 'id_field' => 'uid',
							 'alias_field' => 'title',
							 'addWhereClause' => ' AND NOT deleted',
							 'useUniqueCache' => 1,
							 'useUniqueCache_conf' => array( 
								'strtolower' => 1,
								 'spaceCharacter' => '-',
								 ),
							 'languageGetVar' => 'L',
							 'languageExceptionUids' => '',
							 'languageField' => 'sys_language_uid',
							 'transOrigPointerField' => 'l10n_parent',
							 'autoUpdate' => 1,
							 'expireDays' => 180,
							 ),
						 ),
					 ),
				 // EXT:news end
				),
			 ),
		 'fileName' => array ( 
			
			// if you don't want .html-URLs set the following to "false" (e.g. 'defaultToHTMLsuffixOnPrev' => false,)
			// then you get http://www.yourdomain.com/imprint/ instead of http://www.yourdomain.com/imprint.html
			'defaultToHTMLsuffixOnPrev' => false,
			 'index' => array ( 
				'rss.xml' => array ( 
					'keyValues' => array ( 
						'type' => '100',
						 ),
					 ),
				 'rss091.xml' => array ( 
					'keyValues' => array ( 
						'type' => '101',
						 ),
					 ),
				 'rdf.xml' => array ( 
					'keyValues' => array ( 
						'type' => '102',
						 ),
					 ),
				 'atom.xml' => array ( 
					'keyValues' => array ( 
						'type' => '103',
						 ),
					 ),
				 ),
			 ),
		 ),
	 );

