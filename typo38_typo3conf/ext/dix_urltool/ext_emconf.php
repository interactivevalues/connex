<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "dix_urltool".
 *
 * Auto generated 17-01-2017 09:41
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Urltool',
  'description' => 'Manage realurl configuration',
  'category' => 'module',
  'author' => 'Markus Kappe',
  'author_email' => 'markus.kappe@dix.at',
  'state' => 'beta',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '1.0.1',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '7.6.0-7.6.99',
      'realurl' => '2.1.0-2.99.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => NULL,
);

