config.admPanel=1
plugin.Tx_Formhandler.settings.debug = 0



plugin.Tx_Formhandler.settings {

  predef.formKontaktDE {
    name =  Kontaktformular Default AT-DE

    addErrorAnchors = 1
    #templateFile = fileadmin/reisecenter/templates/ext/formhandler/template.html
    langFile.1 = fileadmin/reisecenter/templates/ext/formhandler/kontakt.xml
    #cssFile.1 = fileadmin/dermacher/templates/ext/formhandler/form.css
  
    # Template file
    templateFile = fileadmin/reisecenter/templates/ext/formhandler/form_simple_contact.html

    # Master template
    masterTemplateFile = fileadmin/reisecenter/templates/ext/formhandler/master_template_contact.html

    formValuesPrefix = formhandler
  
    requiredSign = TEXT
    requiredSign.value = *
    requiredSign.wrap = <span style="color: #FF0000;">|</span>
  
    isErrorMarker.default = class="errorField"
  
    # HTML wrapping by validation error
    errorListTemplate {
      totalWrap = <div class="error"><ul>|</ul></div>
      singleWrap = <li>|</li>
    }
  
    #PreProcessors
    preProcessors {
      1.class = PreProcessor_LoadGetPost
      2.class = PreProcessor_ValidateAuthCode
      2.config {
        #redirectPage = 1
        #errorRedirectPage = 1
        hiddenField = hidden
      }
    }
  
    # Validators configuration
    validators {
      1 {
        class = Tx_Formhandler_Validator_Default
        config {
          fieldConf {
            #gender {
            #  errorCheck.1 = required
            #}
            firstname {
              errorCheck.1 = required
            }
            lastname {
              errorCheck.1 = required
            }
            address {
              errorCheck.1 = required
            }
            zipcity {
              errorCheck.1 = required
            }
            email {
              errorCheck.1 = required
              errorCheck.2 = email
            }
            phone {
              errorCheck.1 = required
            }
            message {
              errorCheck.1 = required
            }
          }
        }
      }
    }
  
    # Loggers configuration
    loggers {
      10 {
        class = Logger\DB
        config {
          disableIPlog = 1
        }
      }
    }
  
    # Finishers configuration
    initInterceptors {
      1 {
        class = Interceptor_CombineFields
        config {
          combineFields {
            subject {
              value = Ihre Anfrage
              fields {
                1 = firstname
                2 = lastname
              }
              separator = -
              hideEmptyValues = 1
            }
          }
        }
      }
    }
  
    # Finishers configuration
    finishers {

      3.class = Tx_Formhandler_Finisher_Mail
      3.config {
        admin {
          to_email = reisegutschein@connexservice.com
          #to_email = chg@connexgroup.net
          bcc_email = chg@connexgroup.net
          to_name = Connex Reisecenter
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Ihre Anfrage / {GP:formhandler|subject}
          sender_email = email
          #sender_name = Connex Markting Group
          htmlEmailAsAttachment = 0
        }
        user {
          to_email = email
          #bcc_email = csr@connexgroup.net
          to_name = name
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Ihre Anfrage / {GP:formhandler|subject}
          sender_email = reisegutschein@connexservice.com
          sender_name = Connex Reisecenter
          replyto_email = reisegutschein@connexservice.com
          replyto_name = TEXT
          replyto_name.value = Connex Reisecenter
          htmlEmailAsAttachment = 0
        }
      }

      4.class = Finisher_Redirect
      4.config {
        #redirectPage = 1
        #redirectPage = 4
      }
  
      10.class = Finisher_SubmittedOK
      10.config.returns = 1
    }

    #debuggers.1 {
    #  class = TypoheadsFormhandlerDebuggerPrintToScreen
    #  config {
    #    sectionWrap = <div class=”debug-section”>|</div>
    #  }
    #}

  }



  predef.formKontaktEN {
    name =  Kontaktformular Default EN

    addErrorAnchors = 1
    #templateFile = fileadmin/reisecenter/templates/ext/formhandler/template.html
    langFile.1 = fileadmin/reisecenter/templates/ext/formhandler/kontakt.xml
    #cssFile.1 = fileadmin/dermacher/templates/ext/formhandler/form.css
  
    # Template file
    templateFile = fileadmin/reisecenter/templates/ext/formhandler/form_simple_contact_EN.html

    # Master template
    masterTemplateFile = fileadmin/reisecenter/templates/ext/formhandler/master_template_contact.html

    formValuesPrefix = formhandler
  
    requiredSign = TEXT
    requiredSign.value = *
    requiredSign.wrap = <span style="color: #FF0000;">|</span>
  
    isErrorMarker.default = class="errorField"
  
    # HTML wrapping by validation error
    errorListTemplate {
      totalWrap = <div class="error"><ul>|</ul></div>
      singleWrap = <li>|</li>
    }
  
    #PreProcessors
    preProcessors {
      1.class = PreProcessor_LoadGetPost
      2.class = PreProcessor_ValidateAuthCode
      2.config {
        #redirectPage = 1
        #errorRedirectPage = 1
        hiddenField = hidden
      }
    }
  
    # Validators configuration
    validators {
      1 {
        class = Tx_Formhandler_Validator_Default
        config {
          fieldConf {
            #gender {
            #  errorCheck.1 = required
            #}
            firstname {
              errorCheck.1 = required
            }
            lastname {
              errorCheck.1 = required
            }
            address {
              errorCheck.1 = required
            }
            zipcity {
              errorCheck.1 = required
            }
            email {
              errorCheck.1 = required
              errorCheck.2 = email
            }
            phone {
              errorCheck.1 = required
            }
            message {
              errorCheck.1 = required
            }
          }
        }
      }
    }
  
    # Loggers configuration
    loggers {
      10 {
        class = Logger\DB
        config {
          disableIPlog = 1
        }
      }
    }
  
    # Finishers configuration
    initInterceptors {
      1 {
        class = Interceptor_CombineFields
        config {
          combineFields {
            subject {
              value = Your request
              fields {
                1 = firstname
                2 = lastname
              }
              separator = -
              hideEmptyValues = 1
            }
          }
        }
      }
    }
  
    # Finishers configuration
    finishers {

      3.class = Tx_Formhandler_Finisher_Mail
      3.config {
        admin {
          to_email = reisegutschein@connexservice.com
         # to_email = chg@connexgroup.net
          bcc_email = chg@connexgroup.net
          to_name = Connex Reisecenter
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Your request / {GP:formhandler|subject}
          sender_email = email
          #sender_name = Connex Markting Group
          htmlEmailAsAttachment = 0
        }
        user {
          to_email = email
          #bcc_email = csr@connexgroup.net
          to_name = name
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Your request / {GP:formhandler|subject}
          sender_email = reisegutschein@connexservice.com
          sender_name = Connex Reisecenter
          replyto_email = reisegutschein@connexservice.com
          replyto_name = TEXT
          replyto_name.value = Connex Reisecenter
          htmlEmailAsAttachment = 0
        }
      }

      4.class = Finisher_Redirect
      4.config {
        #redirectPage = 1
        #redirectPage = 4
      }
  
      10.class = Finisher_SubmittedOK
      10.config.returns = 1
    }

    #debuggers.1 {
    #  class = TypoheadsFormhandlerDebuggerPrintToScreen
    #  config {
    #    sectionWrap = <div class=”debug-section”>|</div>
    #  }
    #}

  }



  predef.formKontaktSK {
    name =  Kontaktformular Default SK

    addErrorAnchors = 1
    #templateFile = fileadmin/reisecenter/templates/ext/formhandler/template.html
    langFile.1 = fileadmin/reisecenter/templates/ext/formhandler/kontakt.xml
    #cssFile.1 = fileadmin/dermacher/templates/ext/formhandler/form.css
  
    # Template file
    templateFile = fileadmin/reisecenter/templates/ext/formhandler/form_simple_contact_SK.html

    # Master template
    masterTemplateFile = fileadmin/reisecenter/templates/ext/formhandler/master_template_contact.html

    formValuesPrefix = formhandler
  
    requiredSign = TEXT
    requiredSign.value = *
    requiredSign.wrap = <span style="color: #FF0000;">|</span>
  
    isErrorMarker.default = class="errorField"
  
    # HTML wrapping by validation error
    errorListTemplate {
      totalWrap = <div class="error"><ul>|</ul></div>
      singleWrap = <li>|</li>
    }
  
    #PreProcessors
    preProcessors {
      1.class = PreProcessor_LoadGetPost
      2.class = PreProcessor_ValidateAuthCode
      2.config {
        #redirectPage = 1
        #errorRedirectPage = 1
        hiddenField = hidden
      }
    }
  
    # Validators configuration
    validators {
      1 {
        class = Tx_Formhandler_Validator_Default
        config {
          fieldConf {
            #gender {
            #  errorCheck.1 = required
            #}
            firstname {
              errorCheck.1 = required
            }
            lastname {
              errorCheck.1 = required
            }
            address {
              errorCheck.1 = required
            }
            zipcity {
              errorCheck.1 = required
            }
            email {
              errorCheck.1 = required
              errorCheck.2 = email
            }
            phone {
              errorCheck.1 = required
            }
            message {
              errorCheck.1 = required
            }
          }
        }
      }
    }
  
    # Loggers configuration
    loggers {
      10 {
        class = Logger\DB
        config {
          disableIPlog = 1
        }
      }
    }
  
    # Finishers configuration
    initInterceptors {
      1 {
        class = Interceptor_CombineFields
        config {
          combineFields {
            subject {
              value = Vaša požiadavka
              fields {
                1 = firstname
                2 = lastname
              }
              separator = -
              hideEmptyValues = 1
            }
          }
        }
      }
    }
  
    # Finishers configuration
    finishers {

      3.class = Tx_Formhandler_Finisher_Mail
      3.config {
        admin {
          to_email = office-cee@connexgroup.net
         # to_email = chg@connexgroup.net
          bcc_email = chg@connexgroup.net
          to_name = Connex Reisecenter
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Vaša požiadavka / {GP:formhandler|subject}
          sender_email = email
          #sender_name = Connex Markting Group
          htmlEmailAsAttachment = 0
        }
        user {
          to_email = email
          #bcc_email = csr@connexgroup.net
          to_name = name
          #subject = Kontaktanfrage
          subject = TEXT
          subject.dataWrap = Vaša požiadavka / {GP:formhandler|subject}
          sender_email = office-cee@connexgroup.net
          sender_name = Connex Reisecenter
          replyto_email = office-cee@connexgroup.net
          replyto_name = TEXT
          replyto_name.value = Connex Reisecenter
          htmlEmailAsAttachment = 0
        }
      }

      4.class = Finisher_Redirect
      4.config {
        #redirectPage = 1
        #redirectPage = 4
      }
  
      10.class = Finisher_SubmittedOK
      10.config.returns = 1
    }

    #debuggers.1 {
    #  class = TypoheadsFormhandlerDebuggerPrintToScreen
    #  config {
    #    sectionWrap = <div class=”debug-section”>|</div>
    #  }
    #}

  }



}





