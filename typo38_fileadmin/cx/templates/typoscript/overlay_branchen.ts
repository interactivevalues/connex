lib.overlay_branchen = COA
lib.overlay_branchen {
  #overlay branchen - versicherung
  10 = COA
  10 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 39
  }
  #10.wrap = <div class="">|</div>
  
  #overlay branchen - energieversorger
  20 = COA
  20 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 41
  }
  #20.wrap = <div class="">|</div>

  #overlay branchen - auto
  30 = COA
  30 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 43
  }
  #30.wrap = <div class="">|</div>

  #overlay branchen - baugewerbe
  40 = COA
  40 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 45
  }
  #40.wrap = <div class="">|</div>

  #overlay branchen - industrie
  50 = COA
  50 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 47
  }
  #50.wrap = <div class="">|</div>

  #overlay branchen - handel
  60 = COA
  60 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 49
  }
  #60.wrap = <div class="">|</div>

  #overlay branchen - großhandel
  70 = COA
  70 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 51
  }
  #70.wrap = <div class="">|</div>

  #overlay branchen - leasingbranche
  80 = COA
  80 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 53
  }
  #80.wrap = <div class="">|</div>

  #overlay branchen - leasingbranche
  90 = COA
  90 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 55
  }
  #90.wrap = <div class="">|</div>

  #overlay branchen - banken / sparkassen
  100 = COA
  100 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 57
  }
  #100.wrap = <div class="">|</div>

  #overlay branchen - pharma
  110 = COA
  110 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 59
  }
  #110.wrap = <div class="">|</div>
}
