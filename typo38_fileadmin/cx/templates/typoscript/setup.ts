config.doctype = html5
config.metaCharset = utf-8
config.no_cache = 0
config.admPanel = 0
config.debug = 0

config.spamProtectEmailAddresses = 2
config.spamProtectEmailAddresses_atSubst = -monkey-tail-
config.spamProtectEmailAddresses_lastDotSubst = -dot-

# replace baseURL with your domain, don't forget to add a trailing slash
# the following 3 lines are used primarily with the RealUrl extension
config.baseURL = http://www.connexgroup.net
config.simulateStaticDocuments = 0
config.tx_realurl_enable = 1

config.language = de
config.locale_all = de_DE

page = PAGE
page.typeNum = 0

page = PAGE
page {
    # body von der root Seite die ID übergeben
  bodyTagCObject = TEXT
  bodyTagCObject.dataWrap = <body id="page-top" class="page_{field:alias//field:uid}" >
}

page.meta {
  #description.data = levelfield :-1, description, slide
  #description.override.field = description     
  #keywords.data = levelfield :-1, keywords, slide
  #keywords.override.field = keywords
  #keywords {
  #  data = page:keywords
  #  ifEmpty (
  #    Case Studies,Verkaufsförderung,Kundenbindung,Mitarbeitermotivation,Gutscheinprogramme,Hotelschecks,Prämienprogramme,Vorteilsprogramme,Mehrwertprogramme,Ideenmanagement,Empfehlungssysteme,Branchenlösungen
  #  )
  revisit-after = 7 days
  robots = index, follow
}

page.10 = FLUIDTEMPLATE
page.10 {
    format = html
    file = fileadmin/cx/templates/layouts/main_layout.html
    partialRootPath = fileadmin/cx/templates/partials/
    layoutRootPath = fileadmin/cx/templates/layouts/
    variables {
        content_main < styles.content.get
        content_main.select.where = colPos = 0
        content_column_1 < styles.content.get
        content_column_1.select.where = colPos = 1
        content_column_2 < styles.content.get
        content_column_2.select.where = colPos = 2
    }
}

page.10.file.stdWrap.cObject = CASE
page.10.file.stdWrap.cObject {
    key.data = levelfield:-1, backend_layout_next_level, slide
    key.override.field = backend_layout
   
    default = TEXT
    default.value = fileadmin/cx/templates/main_1_column_with_menu.html
    1 = TEXT
    1.value = fileadmin/cx/templates/main_1_column_with_menu.html
    2 = TEXT
    2.value = fileadmin/cx/templates/main_1_column_without_menu.html
    3 = TEXT
    3.value = fileadmin/cx/templates/main_2_column_without_menu.html
    4 = TEXT
    4.value = fileadmin/cx/templates/onepage.html
}



# be sure to display website correctly in mobile browsers
page.headerData.10 = TEXT
page.headerData.10.value (
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

  <link rel="apple-touch-icon" sizes="180x180" href="fileadmin/cx/templates/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="fileadmin/cx/templates/images/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="fileadmin/cx/templates/images/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="fileadmin/cx/templates/images/favicon/manifest.json">
  <link rel="mask-icon" href="fileadmin/cx/templates/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="fileadmin/cx/templates/images/favicon/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">

)





### responsive
###########################################################
page.headerData.5 = TEXT
page.headerData.5.value (
  <!-- Bootstrap Core CSS -->
  <link href="http://www.connexgroup.net/fileadmin/cx/templates/css/bootstrap.css" rel="stylesheet">

  <!-- Custom CSS -->
  <!--<link href="http://www.connexgroup.net/fileadmin/templates/css/agency.css" rel="stylesheet">-->
  
  <link href="http://www.connexgroup.net/fileadmin/cx/templates/css/slick.css" rel="stylesheet">
  <link href="http://www.connexgroup.net/fileadmin/cx/templates/css/slick-theme.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->


  <style type="text/css">                                    
  @import url("https://fast.fonts.net/lt/1.css?apiType=css&c=9e68ea0c-de02-4ac5-840e-c285243e202f&fontids=1448929,1448937");
  @font-face{
   font-family:"DIN Next LT W04 Light";
    src:url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448929/a6b05bd7-1f09-4c3e-93a5-8b7d432291b3.eot?#iefix");
    src:url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448929/a6b05bd7-1f09-4c3e-93a5-8b7d432291b3.eot?#iefix") format("eot"),url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448929/8a3ff076-5846-4401-9d7e-10993afac709.woff2") format("woff2"),url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448929/f69bc269-16e5-4f8a-80eb-b049139324fd.woff") format("woff"),url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448929/7742af6c-6711-4ca8-9f62-44005bde2243.ttf") format("truetype");
  }
  @font-face{
    font-family:"DIN Next LT W04 Medium";
    src:url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448937/fd94b7ac-c184-4f5c-a847-0a4785f3e98f.eot?#iefix");
    src:url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448937/fd94b7ac-c184-4f5c-a847-0a4785f3e98f.eot?#iefix") format("eot"),url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448937/cf1948b4-c50b-418b-a059-e8ed8bf0f9f1.woff2") format("woff2"),url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448937/bd983b7f-76bb-4f54-83bb-889fd36d60e6.woff") format("woff"),url("http://www.connexgroup.net/fileadmin/cx/templates/font/1448937/63fb1f94-0e8f-4c85-b559-b0dd27ecbf5a.ttf") format("truetype");
  }                                                       
  </style>
)





### css & js module
###########################################################
page.45 = TEXT
page.45.value (


    <!-- jQuery -->
    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/bootstrap.js"></script>


    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/classie.js"></script>
    <!--<script src="http://www.connexgroup.net/fileadmin/cx/templates/js/cbpAnimatedHeader.js"></script>-->
    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/masonry.pkgd.min.js"></script>
    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/dev.js"></script>

    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/appear.js"></script>
    <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/content_slide.js"></script>

    <!-- Custom Theme JavaScript -->


    <script type="text/javascript">
      $(document).on('ready', function() {
        $(".regular").slick({
          dots: false,
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 2000,
          speed: 3000,
          touchMove: false,
          arrows: true,

          responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 1000
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
      });
    </script>

    <!-- Custom Theme JavaScript -->
    <script src="http://www.connexgroup.net/fileadmin/reisecenter/templates/js/agency.js"></script>
    <script src="http://www.connexgroup.net/fileadmin/reisecenter/templates/js/slick.js"></script>


    <script type="text/javascript">
    // create the back to top button
    $('body').prepend('<a href="#" class="back-to-top"><i class="fa fa-angle-double-up" aria-hidden="true"></i> nach oben</a>');

    var amountScrolled = 300;

    $(window).scroll(function() {
      if ( $(window).scrollTop() > amountScrolled ) {
        $('a.back-to-top').fadeIn('slow');
      } else {
      $('a.back-to-top').fadeOut('slow');
      }
    });

    $('a.back-to-top, a.simple-back-to-top').click(function() {
      $('html, body').animate({
        scrollTop: 0
      }, 700);
      return false;
    });
    </script>

    <script>
      $('#myCarousel').carousel({
        interval:   4000
      });
    </script>


  <script src="http://www.connexgroup.net/fileadmin/cx/templates/js/cookiechoices.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function(event) {
      cookieChoices.showCookieConsentBar('Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden.',
        'Ok', 'Weitere Informationen', '');
    });
  </script>

)





<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/cx/templates/typoscript/setup_page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/cx/templates/typoscript/header.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/cx/templates/typoscript/fields.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/cx/templates/typoscript/overlay_branchen.ts">





### google analytics
###########################################################
page.51 = TEXT
page.51.value (

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1644364-1', 'connexgroup.net');
  ga('send', 'pageview');

</script>

)





page.includeCSS {
    file1 = fileadmin/cx/templates/css/style.css
    file2 = fileadmin/cx/templates/css/style_cx.css
    file3 = fileadmin/cx/templates/css/style_cx_addon.css
}





### overlay impressum
###########################################################
lib.modalImpressumHeader = COA
lib.modalImpressumHeader {
  10 = COA
  10 {
    10 = TEXT
    10.value = <h4 class="modal-title" id="myModalLabelImpressum">Impressum</h4>
  }
}

lib.modalImpressumContent = COA
lib.modalImpressumContent {
  10 = COA
  10 {
    10 = RECORDS
    10.tables = tt_content
    10.source = 163
    
    20 = RECORDS
    20.tables = tt_content
    20.source = 165
  }
}



