### header logo / navigation
###########################################################
lib.headerNav = COA
lib.headerNav {
  10 = COA
  10 {
    # mobile icon
    10 = COA
    10 {
      10 = COA
      10 {
        10 = TEXT
        10.value = <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
      }
      10.wrap = <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">|</button>
      
      20 = COA
      20 {
        10 = COA
        10 {
          10 = TEXT
          #10.value = <a class="navbar-brand page-scroll" href="#page-top"><img src="http://www.connexgroup.net/fileadmin/cx/templates/images/svg_icons/Connex_Marketing_Group_Logo_2016_4c.svg"></a>
          10.value = <a class="navbar-brand" href="http://www.connexgroup.net"><img src="http://www.connexgroup.net/fileadmin/cx/templates/images/svg_icons/Connex_Marketing_Group_Logo_2016_4c.svg"></a>
        }
      }
      20.wrap = <div class="col-xs-12 header_logo">|</div>
    }
    10.wrap = <div class="navbar-header page-scroll">|</div>
    
    # navigation
    20 = COA
    20 {
      10 = COA
      10 {
        10 = TEXT
        10.value = <li class="hidden"><a href="#page-top"></a></li>
        20 = TEXT
        20.value = <li class=""><a class="page-scroll" href="#know-how">Connex Lösungen</a></li>
        30 = TEXT
        30.value = <li class="hidden-sm"><a class="page-scroll" href="#marketing_it">Marketing & IT</a></li>
        40 = TEXT
        40.value = <li class=""><a class="page-scroll" href="#blog">Blog</a></li>
        50 = TEXT
        50.value = <li class=""><a class="page-scroll" href="#branchen">Branchen</a></li>
        60 = TEXT
        60.value = <li class=""><a class="page-scroll" href="#about">Über Uns</a></li>
        #70 = TEXT
        #70.value = <li class=""><a class="page-scroll" href="#">Internationalität</a></li>
        80 = TEXT
        80.value = <li class=""><a class="page-scroll" href="#contact">Kontakt</a></li>
      }
      10.wrap = <ul class="nav navbar-nav navbar-right">|</ul>
    }
    20.wrap = <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">|</div>
  }
  10.wrap = <nav class="navbar navbar-default navbar-fixed-top"><div class="container">|</div></nav>
}



### header image
###########################################################
lib.headerImage = COA
lib.headerImage {
  10 = COA
  10 {
    ### image
    10 = COA
    10 {
    }
    10.wrap = <div class="header-image-image">|</div>
    ### content
    20 = COA
    20 {
      10 = TEXT
      10.value = <h1 class="headline text-center">Welcome to the World of Motivation</h1>
      20 = TEXT
      20.value = <span class="subline text-center">Innovative Lösungen zur Verkaufsförderung, Kundenbindung und Mitarbeitermotivation</span>
      30 = TEXT
      30.value = <div class="button"><div class="btn btn-header-image text-center"><a class="page-scroll" href="#contact">Kontakt</a></div></div>
    }
    20.wrap = <div class="header-image-content">|</div>
  }
  10.wrap = <div class="header-image">|</div>
}
