### case_studies ###
temp.section1 = COA
temp.section1 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 167
    }
    10.wrap = <div class="case-studies text-center">|</div>
    
    # slider
    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 21
    }
    30.wrap =
  }
  10.wrap = <div class="container element_from_top">|</div>
}

### know-how ###
temp.section2 = COA
temp.section2 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 169
    }
    10.wrap = <div class="text-center">|</div>
    
    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 65
    }
    20.wrap = <div class="know-how-teaser text-center">|</div>
  }
  10.wrap = <div class="container element_from_top">|</div>
    
  20 = COA
  20 {
    10 = COA
    10 {
      10 = COA
      10 {
      }
      10.wrap = <div class="cx-triangle"></div>
      
      # cascading grid layout library
      30 = COA
      30 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 23
      }
      30.wrap =      
    }
    10.wrap = <div class="container element_from_top">|</div>
  }
  20.wrap = <div class="know-how-wrap">|</div>

}

### marketing_it ###
temp.section3 = COA
temp.section3 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 171
    }
    10.wrap = <div class="case-studies text-center">|</div>
    
    # slider
    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 29
    }
    20.wrap = <div class="know-how-teaser text-center">|</div>
  }
  10.wrap = <div class="container element_from_top">|</div>

  20 = COA
  20 {
  }
  20.wrap = <div class="marketing-it-image">|</div>
}

### blog ###
temp.section4 = COA
temp.section4 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 173
    }
    10.wrap = <div class="blog text-center">|</div>
    
    # slider
    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 35
    }
    30.wrap =
  }
  10.wrap = <div class="container element_from_top">|</div>
}

### branchen ###
temp.section5 = COA
temp.section5 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 175
    }
    10.wrap = <div class="text-center">|</div>

    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 67
    }
    20.wrap = <div class="branchen-teaser text-center">|</div>
  }
  10.wrap = <div class="container element_from_top">|</div>
    
  20 = COA
  20 {
    10 = COA
    10 {
      10 = COA
      10 {
      }
      10.wrap = <div class="cx-triangle"></div>
    
      # cascading grid layout library
      30 = COA
      30 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 37
      }
      30.wrap = 
    }
    10.wrap = <div class="container element_from_top">|</div>
  }
  20.wrap = <div class="branchen-wrap">|</div>

}

### about ###
temp.section6 = COA
temp.section6 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 177
    }
    10.wrap = <div class="about text-center">|</div>
    
    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 69
    }
    20.wrap = <div class="about-teaser text-center">|</div>    
  }
  10.wrap = <div class="container element_from_top">|</div>
  20 = COA
  20 {
    10 = COA
    10 {
      10 = COA
      10 {
      }
      10.wrap = <div class="cx-triangle"></div>
    
      # spider
      30 = COA
      30 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 61
      }
      30.wrap = <div class="col-xs-12 spider_slide_box">|</div>
    }
    10.wrap = <div class="container">|</div>
  }
  20.wrap = <div class="spider_slide">|</div>
}

### international ###
temp.section7 = COA
temp.section7 {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 179
    }
    10.wrap = <div class="international text-center">|</div>

    20 = COA
    20 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 73
    }
    20.wrap = <div class="international-teaser text-center">|</div>
  }
  10.wrap = <div class="container element_from_top">|</div>

  20 = COA
  20 {
    8 = COA
    8 {
    }
    8.wrap = <div class="cx-triangle"></div>    
    
    # standorte
    10 = COA
    10 {
      10 = COA
      10 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 77
      }
      10.wrap = <div class="col-xs-12 location-at location-wrap element_from_top">|</div>
      20 = COA
      20 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 79
      }
      20.wrap = <div class="col-xs-12 location-de location-wrap element_from_top">|</div>
      30 = COA
      30 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 81
      }
      30.wrap = <div class="col-xs-4 location-uk location-wrap element_from_top">|</div>
      40 = COA
      40 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 83
      }
      40.wrap = <div class="col-xs-4 location-sk location-wrap element_from_top">|</div>
      50 = COA
      50 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 85
      }
      50.wrap = <div class="col-xs-4 location-city location-wrap element_from_top">|</div>
    }
    10.wrap = <div class="container international-locations"><div class="row">|</div></div>
  }
  20.wrap = <div class="international-contact element_from_top">|</div>
}

### contact ###
temp.section8 = COA
temp.section8 {
  10 = COA
  10 {
    8 = COA
    8 {
    }
    8.wrap = <div class="cx-triangle"></div> 
    
    10 = COA
    10 {
      10 = RECORDS
      10.tables = tt_content
      10.source = 181
    }
    10.wrap = <div class="contact text-center">|</div>

    20 = TEXT
    20.value = <p>Füllen Sie dieses Formular aus, wenn Sie weitere Informationen zu den Connex Lösungen oder ein unverbindliches Beratungsgespräch wünschen.</p>
    20.value.wrap = <div class="contact-teaser text-center">|</div>
    
    # container contact address
    30 = COA
    30 {
      # contact content
      10 = COA
      10 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 63
      }
      10.wrap = <div class="col-md-4 col-sm-5 col-xs-12 contact-left">|</div>
      # formhander
      20 = COA
      20 {
        10 = RECORDS
        10.tables = tt_content
        10.source = 75
      }
      20.wrap = <div class="col-md-8 col-sm-7 col-xs-12 contact-form">|</div>
      # footer
      30 = COA
      30 {
        
      }
      30.wrap = <div class="footer-block">|</div>
    }
    30.wrap = <div class="col-xs-12 no-padding-0 contact-block">|</div>
  }
  10.wrap = <div class="container element_from_top">|</div>

  20 = COA
  20 {
    10 = COA
    10 {
      10 = TEXT
      10.value = <li><a href="http://www.connexgroup.net">Home</a></li>
      20 = TEXT
      20.value = <li><a class="page_scroll" href="#contact">Kontakt</a></li>
      30 = TEXT
      30.value = <li><a href="" data-toggle="modal" data-target="#myModalImpressum">Impressum</a></li>
    }
    10.wrap =
  }
  20.wrap = <div class="container element_from_top"><div class="col-xs-12 no-padding-0 footer-block"><ul>|</ul></div></div>
}



lib.fields = COA
lib.fields {
  10 = COA
  10 {
    10 = COA
    10 {
      10 = COA
      10 < temp.section1
    }
    10.wrap = <section id="case_studies" class="section section_1">|</section>
    
    20 = COA
    20 {
      10 = COA
      10 < temp.section2
    }
    20.wrap = <section id="know-how" class="section section_2">|</section>
    
    30 = COA
    30 {
      10 = COA
      10 < temp.section3
    }
    30.wrap = <section id="marketing_it" class="section section_3">|</section>
    
    40 = COA
    40 {
      10 = COA
      10 < temp.section4
    }
    40.wrap = <section id="blog" class="section section_4">|</section>
    
    50 = COA
    50 {
      10 = COA
      10 < temp.section5
    }
    50.wrap = <section id="branchen" class="section section_5">|</section>
    
    60 = COA
    60 {
      10 = COA
      10 < temp.section6
    }
    60.wrap = <section id="about" class="section section_6">|</section>
    
    70 = COA
    70 {
      10 = COA
      10 < temp.section7
    }
    70.wrap = <section id="international" class="section section_7">|</section>
    
    80 = COA
    80 {
      10 = COA
      10 < temp.section8
    }
    80.wrap = <section id="contact" class="section section_8">|</section>
  }
  10.wrap = <div class="content_wrap">|</div>
}