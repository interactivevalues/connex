$(document).ready(function(){

//Elements Fading
  $('.element_from_top').each(function () {
    $(this).appear(function() {
      $(this).delay(150).animate({opacity:1,top:"0px"},1000);
    });  
  });
  
  $('.element_from_bottom').each(function () {
    $(this).appear(function() {
      $(this).delay(150).animate({opacity:1,bottom:"0px"},1000);
    });  
  });
  
  
  $('.element_from_left').each(function () {
    $(this).appear(function() {
      $(this).delay(150).animate({opacity:1,left:"0px"},1000);
    });  
  });
  
  
  $('.element_from_right').each(function () {
    $(this).appear(function() {
      $(this).delay(750).animate({opacity:1,right:"0px"},1500);
    });  
  });
  
  $('.element_fade_in').each(function () {
    $(this).appear(function() {
      $(this).delay(150).animate({opacity:1,right:"0px"},1000);
    });  
  });

  $('.step-by-step').each(function (i) {
      var pauseIt = i*500;
      $(this).appear(function() {
          $(this).delay(pauseIt).delay(150).animate({opacity:1,right:"0px"},1000);
      });
  });

    $('.step-by-step-arrow').each(function (i) {
        var pauseIt = i*250;
        $(this).appear(function() {
            $(this).delay(pauseIt).delay(150).animate({opacity:1,right:"0px"},1000);
        });
    });

});